package com.example.sunetanew;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SecondFragment extends BaseFragment{

    private OnSecondFragmentTransactionListener mListener;
    private boolean resume;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = onCreateView(inflater, container, savedInstanceState, R.layout.second_gfragment);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
        }
        resume = true;
    }

    @SuppressLint("WrongConstant")
    private void setUI(View view) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSecondFragmentTransactionListener) {
            mListener = (OnSecondFragmentTransactionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnSecondFragmentTransactionListener {
        void onSecondFragmentTransactionListener();
    }
}
