package com.example.sunetanew.Network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Souvik on 22/9/2020.
 */
public class ApiClient {
    private static Retrofit retrofit = null;


    // not Used Right now
    public static Retrofit getRetrofitClient_with_out_header() {
        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://sunetra.org")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create());
            retrofit = builder.build();
        }
        return retrofit;
    }


}
