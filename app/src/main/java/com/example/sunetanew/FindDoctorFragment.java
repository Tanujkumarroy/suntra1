package com.example.sunetanew;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sunetanew.Adapter.DoctorListAdapter;
import com.example.sunetanew.ModelClass.DepartmentList;
import com.example.sunetanew.ModelClass.DoctorList.$value;
import com.example.sunetanew.ModelClass.DoctorList.DoctorList;
import com.example.sunetanew.Network.Api;
import com.example.sunetanew.Network.ApiClient;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FindDoctorFragment extends Fragment  {

    Spinner sp_departmentlist;
    Api api;
    ArrayList<String> spinner1=new ArrayList<>();
    RecyclerView recyclerView;
    List<$value> valueList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_finddoctor,container,false);

        sp_departmentlist=view.findViewById(R.id.sp_departmentlist);


        api = ApiClient.getRetrofitClient_with_out_header().create(Api.class);
        Call<DepartmentList> call1= api.DEPARTMENT_LIST_CALL();
        call1.enqueue(new Callback<DepartmentList>() {
            @Override
            public void onResponse(Call<DepartmentList> call, Response<DepartmentList> response) {
                if (response.code()==200){

                }
            // System.out.println(response.body().get$values());
                ArrayList<String> strings = new ArrayList<>();
                for (int i = 0; i < response.body().get$values().size(); i++) {
                    strings.add(response.body().get$values().get(i).getDepartmentName());
                }



                ArrayAdapter aa = new ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,strings);
               aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                sp_departmentlist.setAdapter(aa);

            }

            @Override
            public void onFailure(Call<DepartmentList> call, Throwable t) {

            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=view.findViewById(R.id.rv_doctorlist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //retrofit
        api = ApiClient.getRetrofitClient_with_out_header().create(Api.class);
        Call<DoctorList> call1= api.DOCTOR_LIST_CALL();
        call1.enqueue(new Callback<DoctorList>() {
            @Override
            public void onResponse(Call<DoctorList> call, Response<DoctorList> response) {
               if(response.isSuccessful()){
                    System.out.println(response.body().get$values());
                  valueList=response.body().get$values();
                  recyclerView.setAdapter(new DoctorListAdapter(getActivity(),valueList));

                }
            }

            @Override
            public void onFailure(Call<DoctorList> call, Throwable t) {

                Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

    }



}
