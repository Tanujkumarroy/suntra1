package com.example.sunetanew;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

//import static com.example.zedaxist.activity.LoginActivity.SHARE_PREFS;
//import static com.example.zedaxist.activity.LoginActivity.user_Id;

public class SplashActivity extends AppCompatActivity {

    public  String userId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


     //   SharedPreferences sharedPreferences = getSharedPreferences(SHARE_PREFS, MODE_PRIVATE);
       // userId = sharedPreferences.getString(user_Id, "");


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
             /*   if (userId.isEmpty()) {
                    Intent intentLogin = new Intent(com.example.zedaxist.activity.SplashActivity.this, LoginActivity.class);
                    startActivity(intentLogin);
                    finish();
                } else {
                    Intent intentLogin = new Intent(com.example.zedaxist.activity.SplashActivity.this, DashbordActivity.class);
                    startActivity(intentLogin);
                    finish();
                }   */
            }
        }, 4000);
    }
}