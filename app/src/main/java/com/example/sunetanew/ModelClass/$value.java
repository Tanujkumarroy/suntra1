
package com.example.sunetanew.ModelClass;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class $value {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("DepartmentId")
    @Expose
    private Integer departmentId;
    @SerializedName("DepartmentName")
    @Expose
    private String departmentName;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

}
