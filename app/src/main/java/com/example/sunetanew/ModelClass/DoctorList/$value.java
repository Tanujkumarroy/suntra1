
package com.example.sunetanew.ModelClass.DoctorList;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class $value {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("DoctorId")
    @Expose
    private String doctorId;
    @SerializedName("DoctorName")
    @Expose
    private String doctorName;
    @SerializedName("Specialization")
    @Expose
    private String specialization;
    @SerializedName("Degree")
    @Expose
    private String degree;
    @SerializedName("WeekDay")
    @Expose
    private String weekDay;
    @SerializedName("VisitingTime")
    @Expose
    private String visitingTime;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getVisitingTime() {
        return visitingTime;
    }

    public void setVisitingTime(String visitingTime) {
        this.visitingTime = visitingTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
