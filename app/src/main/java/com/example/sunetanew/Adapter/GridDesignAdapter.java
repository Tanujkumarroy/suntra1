package com.example.sunetanew.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sunetanew.AppointmentHistoryFragment;
import com.example.sunetanew.ContactSunetraFragment;
import com.example.sunetanew.EyeTestFragment;
import com.example.sunetanew.FindDoctorFragment;
import com.example.sunetanew.NewsEventsFragment;
import com.example.sunetanew.R;
import com.example.sunetanew.RuleFragment;
import com.example.sunetanew.SetReminderFragment;
import com.example.sunetanew.TestimonialsFragment;
import com.example.sunetanew.TrackProgressFragment;

import java.util.ArrayList;
import java.util.List;

public class GridDesignAdapter extends RecyclerView.Adapter<GridDesignAdapter.RecyclerViewHolder> {

    private Activity activity;
    private List<String> list_items = new ArrayList<>();

    public GridDesignAdapter(Activity activity) {
        this.activity = activity;
        //this.list_items = list_items;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerViewHolder recyclerViewHolder;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_blocks_list_cell, parent, false);
        recyclerViewHolder = new RecyclerViewHolder(view, activity);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position == 0){
                    holder.imv_icon.setImageResource(R.drawable.search);
                    holder.tv_label.setText("FIND A DOCTOR");

                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    FindDoctorFragment findDoctorFragment=new FindDoctorFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,findDoctorFragment).addToBackStack(null).commit();

                }
                else if(position == 1){
                    holder.imv_icon.setImageResource(R.drawable.eye);
                    holder.tv_label.setText("EYE TEST");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    EyeTestFragment eyeTestFragment=new EyeTestFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,eyeTestFragment).addToBackStack(null).commit();

                }
                else if(position == 2){
                    holder.imv_icon.setImageResource(R.drawable.rule);
                    holder.tv_label.setText("20-20-20 Rule");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    RuleFragment ruleFragment=new RuleFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,ruleFragment).addToBackStack(null).commit();

                }
                else if(position == 3){
                    holder.imv_icon.setImageResource(R.drawable.news);
                    holder.tv_label.setText("NEWS & EVENTS");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    NewsEventsFragment newsEventsFragment=new NewsEventsFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,newsEventsFragment).addToBackStack(null).commit();

                }
                else if(position == 4){
                    holder.imv_icon.setImageResource(R.drawable.track);
                    holder.tv_label.setText("TRACK PROGRESS ");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    TrackProgressFragment trackProgressFragment=new TrackProgressFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,trackProgressFragment).addToBackStack(null).commit();
                }
                else if(position == 5){
                    holder.imv_icon.setImageResource(R.drawable.testimonial_icon);
                    holder.tv_label.setText("TESTIMONIALS");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    TestimonialsFragment testimonialsFragment=new TestimonialsFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,testimonialsFragment).addToBackStack(null).commit();
                }
                else if(position == 6){
                    holder.imv_icon.setImageResource(R.drawable.apoitment_history);
                    holder.tv_label.setText("APPOINTMENT HISTORY");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    AppointmentHistoryFragment appointmentHistoryFragment=new AppointmentHistoryFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,appointmentHistoryFragment).addToBackStack(null).commit();
                }
                else if(position == 7){
                    holder.imv_icon.setImageResource(R.drawable.reminder);
                    holder.tv_label.setText("SET REMINDER");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    SetReminderFragment setReminderFragment=new SetReminderFragment();
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fm_container,setReminderFragment)
                            .addToBackStack(null)
                            .commit();

                }
                else if(position == 8){
                    holder.imv_icon.setImageResource(R.drawable.contact_sunetra);
                    holder.tv_label.setText("CONTACT SUNETRA");
                    AppCompatActivity activity=(AppCompatActivity)v.getContext();
                    ContactSunetraFragment contactSunetraFragment=new ContactSunetraFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fm_container,contactSunetraFragment).addToBackStack(null).commit();
                }
            }
        });

        if(position == 0){
            holder.imv_icon.setImageResource(R.drawable.search);
            holder.tv_label.setText("FINA A DOCTOR");
        }
        else if(position == 1){
            holder.imv_icon.setImageResource(R.drawable.eye);
            holder.tv_label.setText("EYE TEST");
        }
        else if(position == 2){
            holder.imv_icon.setImageResource(R.drawable.rule);
            holder.tv_label.setText("20-20-20 Rule");
        }
        else if(position == 3){
            holder.imv_icon.setImageResource(R.drawable.news);
            holder.tv_label.setText("NEWS & EVENTS");
        }
        else if(position == 4){
            holder.imv_icon.setImageResource(R.drawable.track);
            holder.tv_label.setText("TRACK PROGRESS ");
        }
        else if(position == 5){
            holder.imv_icon.setImageResource(R.drawable.testimonial_icon);
            holder.tv_label.setText("TESTIMONIALS");
        }
        else if(position == 6){
            holder.imv_icon.setImageResource(R.drawable.apoitment_history);
            holder.tv_label.setText("APPOINTMENT HISTORY");
        }
        else if(position == 7){
            holder.imv_icon.setImageResource(R.drawable.reminder);
            holder.tv_label.setText("SET REMINDER");
        }
        else if(position == 8){
            holder.imv_icon.setImageResource(R.drawable.contact_sunetra);
            holder.tv_label.setText("CONTACT SUNETRA");
        }

    }

    @Override
    public int getItemCount() {
        //return list_items.size();
        return 9;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private ImageView imv_icon;
        private TextView tv_label;

        public RecyclerViewHolder(View itemView, final Activity activity) {
            super(itemView);
            imv_icon = itemView.findViewById(R.id.imv_icon);
            tv_label = itemView.findViewById(R.id.tv_label);
        }
    }
}
