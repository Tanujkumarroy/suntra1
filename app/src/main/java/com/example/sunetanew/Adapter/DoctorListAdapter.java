
package com.example.sunetanew.Adapter;

import android.app.Activity;
import android.content.Context;
import android.renderscript.Sampler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sunetanew.ModelClass.DoctorList.$value;
import com.example.sunetanew.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder> {
  //  private ArrayList<$value>modelClassArrayList ;
    List<$value> valueList;
    Context context;

    public DoctorListAdapter(Context context, List<$value> userList) {
        this.context = context;
        this.valueList=userList;
    }

    @NonNull
    @Override
    public DoctorListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.doctor_list_cell,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorListAdapter.ViewHolder holder, int position) {

        $value value=valueList.get(position);
        System.out.println(value);

        holder.docter_name.setText(value.getDoctorName().toString());
        holder.doctor_degree.setText(value.getDegree());
        holder.specialist.setText(value.getSpecialization());

     //  Picasso.with(context)

          //     .load(value.getImageUrl())
          //     .into(holder.doctor_image);
        String url = "https://sunetra.org/"+value.getImageUrl();

        Glide.with(context).load(url).into(holder.doctor_image);



      //  holder.doctor_image.setImageResource(Integer.parseInt(value.getImageUrl()));
    }

    @Override
    public int getItemCount() {
        return valueList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView docter_name,doctor_degree,specialist;
        ImageView doctor_image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            docter_name=itemView.findViewById(R.id.docter_name);
            doctor_degree=itemView.findViewById(R.id.doctor_degree);
            specialist=itemView.findViewById(R.id.specialist);
            doctor_image=itemView.findViewById(R.id.doctor_image);

        }
    }
}

